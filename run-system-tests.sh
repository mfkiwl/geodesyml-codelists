#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << EOF
Usage: $script <env>
where
    env is deployment environment, eg., dev, test, or prod
EOF
}

environment=$1

if [ -z "$environment" ]; then
    echo "Error: deployment environment is unspecified"
    printUsage
    exit 1
fi

# AWS Java SDK looks for AWS_REGION, not AWS_DEFAULT_REGION
export AWS_REGION=$AWS_DEFAULT_REGION

# Run system tests only
mvn -Denv="$environment" verify -P system-tests
