variable "region" {}

variable "environment" {
  description = "Deployment environment."
}

variable "enable_cloudwatch_event_rule" {
  description = "Whether the cloudwatch event rule should be enabled. Eg., only anable the rule for dev or prod."
}
