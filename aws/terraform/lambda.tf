locals {
  prefix = "geodesyml-codelists"

  repo_root_path   = "${path.root}/../.."
  lambda_file_path = "${local.repo_root_path}/target/geodesyml-codelists-upload-handler-lambda.jar"
}

resource "aws_lambda_function" "codelist_upload_handler" {
  filename         = local.lambda_file_path
  function_name    = "${local.prefix}-upload-handler-${var.environment}"
  role             = aws_iam_role.codelist_lambda.arn
  handler          = "au.gov.ga.gnss.geodesymlcodelist.support.aws.GmxCodelistUploadHandlerLambda"
  source_code_hash = filebase64sha256(local.lambda_file_path)
  runtime          = "java8"
  memory_size      = 512
  timeout          = 300

  environment {
    variables = {
      codelists_bucket_name = aws_s3_bucket.codelists.id
    }
  }
}

resource "aws_lambda_permission" "codelist_upload_handler" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.codelist_upload_handler.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.codelist_upload.arn
}

resource "aws_cloudwatch_event_rule" "codelist_upload" {
  name        = "${local.prefix}-upload-trigger-${var.environment}"
  description = "Generate and upload geodesyml codelists to s3 bucket at 10:00am each day"

  schedule_expression = "cron(0 0 * * ? *)"
  is_enabled          = var.enable_cloudwatch_event_rule
}

resource "aws_cloudwatch_event_target" "trigger_codelist_upload" {
  rule = aws_cloudwatch_event_rule.codelist_upload.name
  arn  = aws_lambda_function.codelist_upload_handler.arn
}

output "codelist_lambda_arn" {
  value = aws_lambda_function.codelist_upload_handler.arn
}
