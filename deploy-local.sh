#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"

function usage {
    echo "Usage: $script"
    exit 1
}

if [[ $# -gt 0 ]]; then
    echo "No argument excepted"
    usage
fi

environment="local"
export TF_VAR_region=$AWS_DEFAULT_REGION
export TF_VAR_environment=$environment

cd "$(dirname "$(realpath "$0")")"
terraform_workspace="$(pwd)/aws/terraform/workspaces/$environment"
cd aws/terraform

rm -frv terraform.tfstate.d/$environment/terraform.tfstate
rm -frv .terraform

cat > main_override.tf << EOF
terraform {
  required_version = "~> 0.12.0"
  backend "local" {}
}
provider "aws" {
  version = "~> 2.22.0"
  s3_force_path_style = true
  endpoints {
    s3 = "http://localhost:4572"
  }
}
EOF

terraform init
terraform workspace select $environment || terraform workspace new $environment
terraform apply -auto-approve -input=false \
                -var-file="$terraform_workspace/terraform.tfvars" \
                -target aws_s3_bucket.codelists

function onExit {
    rm -f main_override.tf
}

trap onExit EXIT ERR
trap "onExit; exit 1" SIGINT
