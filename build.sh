#!/usr/bin/env bash

set -e

# AWS Java SDK looks for AWS_REGION
export AWS_REGION=$AWS_DEFAULT_REGION
skipTestsOption=${1:-skipSystemTests}

# Build, test and verify all Java components
mvn clean install "-D$skipTestsOption"
