# GeodesyML Codelists

## About

GeodesyML Codelists is a Java-based web service for extracting GNSS antenna-radome and
receiver types from external resources. Data extracted are outputted in XML format using
GMX Codelists schema: https://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml.

Antenna and radome type combinations are sourced from

* https://files.igs.org/pub/station/general/igs14.atx
* https://www.ngs.noaa.gov/ANTCAL/LoadFile?file=ngs14.atx
* http://ftp.aiub.unibe.ch/BSWUSER52/GEN/PCV_COD.I14

and published daily at

* https://geodesyml-codelists-prod.s3-ap-southeast-2.amazonaws.com/GNSS-AntennaRadome-Codelists.xml

Receiver types are sourced from

* http://ftp.aiub.unibe.ch/BSWUSER52/GEN/RECEIVER.
* https://files.igs.org/pub/station/general/rcvr_ant.tab

and published daily at

* https://geodesyml-codelists-prod.s3-ap-southeast-2.amazonaws.com/GNSS-Receiver-Codelists.xml

## Building a Development Environment

This project uses [Nix](https://nixos.org/nix) to manage build environments for both development
workstations and BitBucket Pipelines. See geoscienceaustralia/project0-nix.

1) Install Nix

```bash
curl https://nixos.org/nix/install | sh
```
2) In your projects root directory, enter nix shell to download and install the required
software as specified in `./shell.nix`

```bash
nix-shell
```

## Managing Nix Dependencies

Edit `shell.nix` to add or remove dependencies. Use [Niv](https://github.com/nmattia/niv) to manage
[nixpkgs](https://github.com/NixOS/nixpkgs) and any other sources of nix expressions. Regularly cache
any changes to project dependencies by manually running custom pipeline `build-pipelines-docker-image`.
