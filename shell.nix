{
  pkgs ? import ./nix {},
  installDevTools ? true
}:

let 
  projectName = "geodesyml-codelists";

  buildTools = with pkgs; [
    cacert
    awscli
    docker
    jdk8
    maven
    python3Packages.credstash
    terraform_0_12
  ];

  devTools = with pkgs; [
    # eclipses.eclipse-java
    niv
  ];

  env = pkgs.buildEnv {
    name = projectName + "-env";
    paths = buildTools ++ (
      if installDevTools then devTools else []
    );
  };

in
  pkgs.mkShell {
    buildInputs = [
      env
    ];
    shellHook = ''
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
      export PROJECT_NAME=${projectName}
      if [ -e "./aws/aws-env.sh" ]; then
        . ./aws/aws-env.sh
      fi
    '';
  }
