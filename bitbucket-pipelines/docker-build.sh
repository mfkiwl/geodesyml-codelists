#!/usr/bin/env bash

set -euo pipefail

organization=geoscienceaustralia
repository="$PROJECT_NAME-bitbucket-pipelines"
username=geodesyarchive
# TODO: Rename geodesy_archive to gnss_informatics
passwordKey=geodesy_archive_docker_hub_key
push=false

while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--push)
            push=true
            shift
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

cd "$(dirname "${BASH_SOURCE[0]}")"

credstash get $passwordKey | docker login -u "$username" --password-stdin

docker build -t "$organization/$repository" -f Dockerfile ..

if [[ $push = "true" ]]; then
    docker push "$organization/$repository"
fi
