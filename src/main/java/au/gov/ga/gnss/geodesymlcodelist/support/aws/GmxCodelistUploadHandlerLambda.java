package au.gov.ga.gnss.geodesymlcodelist.support.aws;

import au.gov.ga.gnss.geodesymlcodelist.codelist.Codelist;
import au.gov.ga.gnss.geodesymlcodelist.codelist.AntennaRadomeCodelist;
import au.gov.ga.gnss.geodesymlcodelist.codelist.ReceiverCodelist;
import au.gov.ga.gnss.geodesymlcodelist.support.gmx.GmxCodelistCreator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GmxCodelistUploadHandlerLambda implements RequestHandler<AwsProxyRequest, AwsLambdaEmptyResponse> {

    @Autowired
    private AmazonS3 s3Client;

    @Autowired
    private String bucketName;

    private GmxCodelistCreator gmxCodelistCreator;

    public GmxCodelistUploadHandlerLambda() {
        gmxCodelistCreator = new GmxCodelistCreator();
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(SpringContextConfiguration.class);
        context.refresh();
        this.s3Client = (AmazonS3) context.getBean("s3Client");
        this.bucketName = (String) context.getBean("bucketName");
    }

    @Override
    public AwsLambdaEmptyResponse handleRequest(AwsProxyRequest proxyRequest, Context context) {
        List<Codelist> codelists = this.getCodelists();
        for (Codelist codelist : codelists) {
            String codeName = codelist.getCodeName();
            String outputGmxFilename = "GNSS-" + codeName + "-Codelists.xml";
            try {
                Set<CodeDefinitionType> codes = codelist.getAllCodes();
                log.info("Total number of " + codeName + " codelists: " + codes.size());

                String gmxContent = gmxCodelistCreator.getGmxCodelistContent(codeName, codes);
                InputStream inputStream = new ByteArrayInputStream(gmxContent.getBytes());

                ObjectMetadata objectMetadata = new ObjectMetadata();
                objectMetadata.setContentLength(gmxContent.length());
                objectMetadata.setContentType("application/xml");

                PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucketName,
                    outputGmxFilename, inputStream, objectMetadata)
                        .withCannedAcl(CannedAccessControlList.PublicRead);

                s3Client.putObject(putObjectRequest);
                log.info("Upload " + codeName + " codelist to S3 bucket " + this.bucketName + ": successful");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return new AwsLambdaEmptyResponse();
    }

    private List<Codelist> getCodelists() {
        ArrayList<Codelist> codelists = new ArrayList<>();
        codelists.add(new AntennaRadomeCodelist());
        codelists.add(new ReceiverCodelist());
        return codelists;
    }

    public static class SpringContextConfiguration {
        @Bean
        public AmazonS3 s3Client() {
            return AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withRegion(Regions.AP_SOUTHEAST_2)
                .build();
        }

        @Value("${codelists_bucket_name}")
        private String bucketName;

        @Bean
        public String bucketName() {
            return this.bucketName;
        }
    }
}
