package au.gov.ga.gnss.geodesymlcodelist.support.aws;

/**
 * Empty AWS Lambda response type
 */
public class AwsLambdaEmptyResponse {
}
