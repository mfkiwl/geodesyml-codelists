package au.gov.ga.gnss.geodesymlcodelist.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.Set;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

/**
 * Parses I14 files and extracts GNSS antenna-radome types
 */
public class AntennaRadomeTypeI14Parser extends GnssTypeParser {

    private static final String KEYWORD = "ANTENNA/RADOME TYPE";

    @Override
    protected String getCodeTypeName() {
        return "AntennaRadomeType";
    }

    @Override
    public Set<CodeDefinitionType> parse(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        Set<CodeDefinitionType> codes = new TreeSet<>(Comparator.comparing(CodeDefinitionType::getId));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.contains(KEYWORD)) {
                bufferedReader.readLine();  // skip the second line with *
                line = bufferedReader.readLine();
                CodeDefinitionType code = getCodeDefinitionType(line, true);
                if (code != null) {
                    codes.add(code);
                }
            }
        }

        return codes;
    }
}
