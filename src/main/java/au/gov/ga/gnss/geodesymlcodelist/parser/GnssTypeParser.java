package au.gov.ga.gnss.geodesymlcodelist.parser;

import net.opengis.gml.v_3_2_1.CodeWithAuthorityType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

import java.io.Reader;
import java.io.IOException;
import java.util.Set;

/**
 * An abstract super class for parsing & extracting code definitions from input files.
 */
public abstract class GnssTypeParser {
    private static final String CODE_SPACE = "urn:xml-gov-au:icsm:egeodesy";
    private static final int CODE_LENGTH = 20;

    /**
     * Returns the name of the code to be parsed
     */
    protected abstract String getCodeTypeName();

    /**
     * Parses code definitions from the input reader
     *
     * @param reader an instance of input Reader from a url or a file
     * @return a list of CodeDefinitionType instances
     * @throws IOException
     */
    public abstract Set<CodeDefinitionType> parse(Reader reader) throws IOException;

    /**
     * Parses the code definition from the input line and converts it into an instance of type CodeDefinitionType.
     *
     * @param line a line read from the input file
     * @param isStrictLengthMatch the length of the code must exactly match the maximum length specified
     * @return an instance of type CodeDefinitionType
     */
    protected CodeDefinitionType getCodeDefinitionType(String line, boolean isStrictLengthMatch) {
        if (line.length() < CODE_LENGTH) {
            return null;
        }

        // for rcvr_ant.tab file
        if (line.startsWith("+--")) {    // skip tab border "+-------"
            return null;
        } else if (line.startsWith("| ")) {    // first column between two vertical bars "|"
            int midBarIndex = line.indexOf("|", 2);
            if (midBarIndex == -1) {
                return null;
            }
            line = line.substring(2, midBarIndex).trim();
        }

        int codeLength = Math.min(line.length(), CODE_LENGTH);
        String code = line.substring(0, codeLength).trim().toUpperCase();
        if (code.length() == 0 || (isStrictLengthMatch && code.length() != CODE_LENGTH)) {
            return null;
        } else if (code.contains("XXXXXXXX")) {    // exclude "JPS XXXXXXXX", "ASHTECH XXXXXXXX"
            return null;
        }

        String id = this.getCodeTypeName() + "Code_" + code.replaceAll("\\s+", "_");
        return (CodeDefinitionType) new CodeDefinitionType()
            .withId(id)
            .withIdentifier(new CodeWithAuthorityType()
                .withValue(code)
                .withCodeSpace(CODE_SPACE)
            );
    }
}
