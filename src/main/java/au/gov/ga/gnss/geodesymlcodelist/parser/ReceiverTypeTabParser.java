package au.gov.ga.gnss.geodesymlcodelist.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.Set;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

/**
 * Parses ascii TAB files and extracts GNSS receiver types
 */
public class ReceiverTypeTabParser extends GnssTypeParser {

    private static final String START_KEYWORD_A = "Receivers";
    private static final String START_KEYWORD_B = "Rcvrs";
    private static final String START_KEYWORD_C = "Previously valid";
    private static final String END_KEYWORD = "+-------------------";

    @Override
    protected String getCodeTypeName() {
        return "ReceiverType";
    }

    @Override
    public Set<CodeDefinitionType> parse(Reader reader) throws IOException {
        Set<CodeDefinitionType> codes = new TreeSet<>(Comparator.comparing(CodeDefinitionType::getId));
        String line;
        BufferedReader bufferedReader = new BufferedReader(reader);
        boolean isInDataTable = false;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.contains(START_KEYWORD_A) || line.contains(START_KEYWORD_B)) {
                bufferedReader.readLine();  // skip 3 lines
                bufferedReader.readLine();
                isInDataTable = bufferedReader.readLine().startsWith(END_KEYWORD); // 3rd line is header border line
                continue;
            } else if (isInDataTable && line.startsWith(END_KEYWORD)) {
                isInDataTable = false;
                line = bufferedReader.readLine();
                if (line != null && line.contains(START_KEYWORD_C)) { // start to parse "Previously valid" receivers
                    isInDataTable = true;
                    line = bufferedReader.readLine();  // skip a separate line
                }
            }

            if (line.contains(END_KEYWORD)) {
                continue;
            }

            if (isInDataTable) {
                CodeDefinitionType code = getCodeDefinitionType(line, false);
                if (code != null) {
                    codes.add(code);
                }
            }
        }

        return codes;
    }
}
