package au.gov.ga.gnss.geodesymlcodelist.support.jaxb;

import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

import net.opengis.gml.v_3_2_1.DictionaryType;
import net.opengis.iso19139.gmx.v_20070417.CodeListDictionaryType;
import net.opengis.iso19139.gmx.v_20070417.ObjectFactory;

/**
 * JAXB marshalling and unmarshalling of ISO19139 entity type {@code gmx:CodeListDictionaryType}.
 */
public class GmxCodelistMarshaller {

    private JAXBContext jaxbContext;
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    private ObjectFactory objectFactory = new ObjectFactory();

    public GmxCodelistMarshaller() {
        try {
            this.jaxbContext = JAXBContext.newInstance(
                CodeListDictionaryType.class.getPackage().getName()
            );

            this.marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            this.configureNamespacePrefixMapping(marshaller);

            this.unmarshaller = this.jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException("Failed to initialise JAXBContext", e);
        }
    }

    public void marshal(CodeListDictionaryType dictionary, Writer writer) throws JAXBException {
        this.marshaller.marshal(this.objectFactory.createCodeListDictionary(dictionary), writer);
    }

    public DictionaryType unmarshal(Reader reader) throws JAXBException {
        JAXBElement<?> element = (JAXBElement<?>) this.unmarshaller.unmarshal(reader);
        return (DictionaryType) element.getValue();
    }

    private void configureNamespacePrefixMapping(Marshaller marshaller) throws PropertyException {
        marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapper() {

            @SuppressWarnings("serial")
            private Map<String, String> namespacePrefixMap = new HashMap<String, String>() {{
                put("http://www.w3.org/1999/xlink", "xlink");
                put("http://www.opengis.net/gml/3.2", "gml");
                put("http://www.isotc211.org/2005/gco", "gco");
                put("http://www.isotc211.org/2005/gmd", "gmd");
                put("http://www.isotc211.org/2005/gmx", "gmx");
                put("http://www.isotc211.org/2005/gts", "gts");
            }};

            public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
                return namespacePrefixMap.getOrDefault(namespaceUri, suggestion);
            }
        });
    }
}
