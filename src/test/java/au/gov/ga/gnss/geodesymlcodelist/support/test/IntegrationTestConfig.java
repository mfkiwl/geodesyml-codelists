package au.gov.ga.gnss.geodesymlcodelist.support.test;

import au.gov.ga.gnss.geodesymlcodelist.support.aws.GmxCodelistUploadHandlerLambda;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration for integration tests.
 */
@Configuration
public class IntegrationTestConfig {

    @Bean
    public AmazonS3 s3Client() {
        return AmazonS3ClientBuilder
            .standard()
            .withPathStyleAccessEnabled(true)
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:4572", "ap-southeast-2"))
            .disableChunkedEncoding()
            .build();
    }

    @Bean
    public String codelistBucketName() {
        return "geodesyml-codelists-local";
    }

    @Bean
    public GmxCodelistUploadHandlerLambda gmxCodelistUploadHandlerLambda() {
        return new GmxCodelistUploadHandlerLambda();
    }
}
