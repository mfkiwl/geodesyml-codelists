package au.gov.ga.gnss.geodesymlcodelist.support.test;

import au.gov.ga.gnss.geodesymlcodelist.support.aws.GmxCodelistUploadHandlerLambda;
import au.gov.ga.gnss.geodesymlcodelist.support.jaxb.GmxCodelistMarshaller;

import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXBElement;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.internal.SkipMd5CheckStrategy;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionPropertyType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;
import net.opengis.iso19139.gmx.v_20070417.CodeListDictionaryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import lombok.extern.slf4j.Slf4j;

/**
 * Base class for all integration tests.
 */
@ContextConfiguration(
    classes = { IntegrationTestConfig.class }
)
@Slf4j
public class IntegrationTest extends AbstractTestNGSpringContextTests {

    private Date dateInvocation;
    private GmxCodelistMarshaller marshaller;

    @Autowired
    public AmazonS3 s3Client;

    @Autowired
    @Qualifier("codelistBucketName")
    public String bucketName;

    @Autowired
    public GmxCodelistUploadHandlerLambda gmxCodelistUploadHandlerLambda;

    @BeforeClass
    public void setUp() {
        dateInvocation = new Date();
        marshaller = new GmxCodelistMarshaller();

        // Fix SdkClientException: Unable to verify integrity of data download
        // https://github.com/localstack/localstack/issues/869
        System.setProperty(SkipMd5CheckStrategy.DISABLE_GET_OBJECT_MD5_VALIDATION_PROPERTY,"true");
    }

    @BeforeMethod
    public void printTestMethod(Method testMethod) {
        log.info("Running test method " + getClass().getSimpleName() + "." + testMethod.getName());
    }

    public boolean checkExistenceOfCodelistXml(String codeName) {
        String key = this.getCodelistObjectKey(codeName);
        ObjectMetadata objectMetadata = s3Client.getObjectMetadata(this.bucketName, key);
        Date dateModified = objectMetadata.getLastModified();
        return dateModified.after(dateInvocation);
    }

    public List<CodeDefinitionPropertyType> getCodesFromCodelistXml(String codeName) throws Exception {
        String key = this.getCodelistObjectKey(codeName);
        S3Object object = s3Client.getObject(this.bucketName, key);
        S3ObjectInputStream inputStream = object.getObjectContent();
        Reader reader = new InputStreamReader(inputStream);
        CodeListDictionaryType codelistDictionary = (CodeListDictionaryType)marshaller.unmarshal(reader);
        List<CodeDefinitionPropertyType> codes = codelistDictionary.getCodeEntry();
        log.info("Number of " + codeName + " code types retrieved: " + codes.size());
        return codes;
    }

    public String getCodeValue(CodeDefinitionPropertyType codeDefinitionPropertyType) {
        JAXBElement jaxbElement = codeDefinitionPropertyType.getCodeDefinition();
        CodeDefinitionType codeType = (CodeDefinitionType)jaxbElement.getValue();
        String codeValue = codeType.getIdentifier().getValue();
        log.info("The first code value parsed: " + codeValue);
        return codeValue;
    }

    private String getCodelistObjectKey(String codeName) {
        return "GNSS-" + codeName + "-Codelists.xml";
    }
}
