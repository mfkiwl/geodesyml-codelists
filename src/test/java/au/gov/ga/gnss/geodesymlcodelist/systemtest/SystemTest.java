package au.gov.ga.gnss.geodesymlcodelist.systemtest;

import au.gov.ga.gnss.geodesymlcodelist.support.jaxb.GmxCodelistMarshaller;

import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBElement;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionPropertyType;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;
import net.opengis.iso19139.gmx.v_20070417.CodeListDictionaryType;

import lombok.extern.slf4j.Slf4j;

/**
 * Base class for system tests.
 */
@ContextConfiguration(
    classes = {
        SystemTestConfig.class,
    },
    loader = AnnotationConfigContextLoader.class
)
@Slf4j
public abstract class SystemTest extends AbstractTestNGSpringContextTests {

    private Date dateInvocation;

    @Autowired
    private AmazonS3 s3Client;

    @Autowired
    private AWSLambda lambdaClient;

    @Autowired
    @Qualifier("codelistBucketName")
    private String bucketName;

    @Autowired
    @Qualifier("codelistLambdaFunctionName")
    private String lambdaFunctionName;

    @BeforeMethod
    public void printTestMethod(Method testMethod) {
        log.info("Running test method " + getClass().getSimpleName() + "." + testMethod.getName());
    }

    private GmxCodelistMarshaller marshaller = new GmxCodelistMarshaller();

    public int invokeCodelistLambda() {
        dateInvocation = new Date();
        InvokeRequest request = new InvokeRequest()
            .withFunctionName(lambdaFunctionName)
            .withInvocationType(InvocationType.RequestResponse);
        InvokeResult invokeResult = lambdaClient.invoke(request);
        log.info("Status Code = " + invokeResult.getStatusCode() + ".");
        return invokeResult.getStatusCode();
    }

    public boolean checkExistenceOfCodelistXml(String codeName) {
        String key = this.getCodelistObjectKey(codeName);
        ObjectMetadata objectMetadata = s3Client.getObjectMetadata(this.bucketName, key);
        Date dateModified = objectMetadata.getLastModified();
        return dateModified.after(dateInvocation);
    }

    public List<CodeDefinitionPropertyType> getCodesFromCodelistXml(String codeName) throws JAXBException {
        String key = this.getCodelistObjectKey(codeName);
        S3Object object = s3Client.getObject(this.bucketName, key);
        S3ObjectInputStream inputStream = object.getObjectContent();
        CodeListDictionaryType codelistDictionary = (CodeListDictionaryType)marshaller.unmarshal(new InputStreamReader(inputStream));
        List<CodeDefinitionPropertyType> codes = codelistDictionary.getCodeEntry();
        log.info("Number of " + codeName + " code types retrieved: " + codes.size());
        return codes;
    }

    public String getCodeValue(CodeDefinitionPropertyType codeDefinitionPropertyType) {
        JAXBElement jaxbElement = codeDefinitionPropertyType.getCodeDefinition();
        CodeDefinitionType codeType = (CodeDefinitionType)jaxbElement.getValue();
        String codeValue = codeType.getIdentifier().getValue();
        log.info("The first code value parsed: " + codeValue);
        return codeValue;
    }

    private String getCodelistObjectKey(String codeName) {
        return "GNSS-" + codeName + "-Codelists.xml";
    }
}
