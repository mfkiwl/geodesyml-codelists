package au.gov.ga.gnss.geodesymlcodelist.systemtest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * Spring context configuration for system tests.
 */
@Configuration
@ComponentScan("au.gov.ga.gnss.geodesymlcodelist.systemtest")
public class SystemTestConfig {

    @Value("${env}")
    private String environment;

    @Bean
    public String codelistBucketName() {
        return "geodesyml-codelists-" + this.environment;
    }

    @Bean
    public String codelistLambdaFunctionName() {
        return "geodesyml-codelists-upload-handler-" + this.environment;
    }

    @Bean
    public AmazonS3 s3Client() {
        return AmazonS3ClientBuilder
            .standard()
            .build();
    }

    @Bean
    public AWSLambda lambdaClient() {
        return AWSLambdaClientBuilder
                .standard()
                .build();
    }
}
