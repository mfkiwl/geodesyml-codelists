package au.gov.ga.gnss.geodesymlcodelist.parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionType;

@Slf4j
public class AntennaRadomeTypeParsersTest {

    private static final String TEST_DATA_DIR = "data/";

    private Set<CodeDefinitionType> igsAtxCodes;
    private Set<CodeDefinitionType> ngsAtxCodes;
    private Set<CodeDefinitionType> pcvI14Codes;

    @Test
    public void testAntennaRadomeTypeIgsAtxParser() throws IOException {
        String filename = "igs14.atx";
        AntennaRadomeTypeAtxParser atxParser = new AntennaRadomeTypeAtxParser();
        try (Reader reader = this.getTestFile(filename)) {
            igsAtxCodes = atxParser.parse(reader);
        }
        log.info("Number of Antenna-Radome types found from " + filename + ": " + igsAtxCodes.size());
        assertThat(igsAtxCodes.size()).isEqualTo(6);
    }

    @Test
    public void testAntennaRadomeTypeNgsAtxParser() throws IOException {
        String filename = "ngs14.atx";
        AntennaRadomeTypeAtxParser atxParser = new AntennaRadomeTypeAtxParser();
        try (Reader reader = this.getTestFile(filename)) {
            ngsAtxCodes = atxParser.parse(reader);
        }
        log.info("Number of Antenna-Radome types found from " + filename + ": " + ngsAtxCodes.size());
        assertThat(ngsAtxCodes.size()).isEqualTo(11);
    }

    @Test
    public void testAntennaRadomeTypeAiubParser() throws IOException {
        String filename = "PCV_COD.I14";
        AntennaRadomeTypeI14Parser i14Parser = new AntennaRadomeTypeI14Parser();
        try (Reader reader = this.getTestFile(filename)) {
            pcvI14Codes = i14Parser.parse(reader);
        }
        log.info("Number of Antenna-Radome types found from " + filename + ": " + pcvI14Codes.size());
        assertThat(pcvI14Codes.size()).isEqualTo(12);
    }

    @Test(dependsOnMethods = {
        "testAntennaRadomeTypeIgsAtxParser",
        "testAntennaRadomeTypeNgsAtxParser",
        "testAntennaRadomeTypeAiubParser"
    })
    private void testAllAntennaRadomeTypeParsers() {
        int maxCount = igsAtxCodes.size() + ngsAtxCodes.size() + pcvI14Codes.size();
        int minCount = Math.max(Math.max(igsAtxCodes.size(), ngsAtxCodes.size()), pcvI14Codes.size());
        igsAtxCodes.addAll(ngsAtxCodes);
        igsAtxCodes.addAll(pcvI14Codes);
        log.info("Total number of antenna-radome types after merge: " + igsAtxCodes.size());
        assertThat(igsAtxCodes.size()).isBetween(minCount, maxCount);
    }

    private Reader getTestFile(String filename) {
        String filePath = this.TEST_DATA_DIR + filename;
        return new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath));
    }
}
