package au.gov.ga.gnss.geodesymlcodelist.systemtest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import javax.xml.bind.JAXBException;
import net.opengis.iso19139.gmx.v_20070417.CodeDefinitionPropertyType;
import org.testng.annotations.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CodelistUploadLambdaSystemTest extends SystemTest {

    private static final int HTTP_STATUS_OK = 200;

    // Number of codes retrieved from inputs as on 09/09/2019.
    private static final int MAX_ANTENNA_RADOME_CODES = 963;
    private static final int MAX_RECEIVER_CODES = 450;

    @Test
    public void testInvokeCodelistLambda() {
        int statusCode = this.invokeCodelistLambda();
        assertThat(statusCode).isEqualTo(HTTP_STATUS_OK);
    }

    @Test(dependsOnMethods = {
        "testInvokeCodelistLambda"
    })
    public void checkAntennaRadomeCodelistXmlUploaded() {
        String codeName = "AntennaRadome";
        assertThat(this.checkExistenceOfCodelistXml(codeName)).isTrue();
    }

    @Test(dependsOnMethods = {
        "testInvokeCodelistLambda"
    })
    public void checkReceiverCodelistXmlUploaded() {
        String codeName = "Receiver";
        assertThat(this.checkExistenceOfCodelistXml(codeName)).isTrue();
    }

    @Test(dependsOnMethods = {
        "checkAntennaRadomeCodelistXmlUploaded"
    })
    public void checkAntennaRadomeCodelistContent() throws JAXBException {
        String codeName = "AntennaRadome";
        List<CodeDefinitionPropertyType> codes = this.getCodesFromCodelistXml(codeName);
        assertThat(codes.size()).isGreaterThanOrEqualTo(MAX_ANTENNA_RADOME_CODES);

        CodeDefinitionPropertyType firstCodeType = codes.get(0);
        String firstCodeValue = this.getCodeValue(firstCodeType);
        assertThat(firstCodeValue.length()).isEqualTo(20);
    }

    @Test(dependsOnMethods = {
        "checkReceiverCodelistXmlUploaded"
    })
    public void checkReceiverCodelistContent() throws JAXBException {
        String codeName = "Receiver";
        List<CodeDefinitionPropertyType> codes = this.getCodesFromCodelistXml(codeName);
        assertThat(codes.size()).isGreaterThanOrEqualTo(MAX_RECEIVER_CODES);

        CodeDefinitionPropertyType firstCodeType = codes.get(0);
        String firstCodeValue = this.getCodeValue(firstCodeType);
        assertThat(firstCodeValue).isNotEmpty();
    }
}
