# Testing data for **GeodesyML Codelists**

## About
The src/test/resources/data/ directory contains text files of various formats for testing.
These testing files are small portions of the real input data from external internet resources.

## Source links of testing files
There are five testing files containing code definitions for GNSS antenna-radome and/or receiver types:

1) igs14.atx  
Source link: [https://files.igs.org/pub/station/general/igs14.atx](https://files.igs.org/pub/station/general/igs14.atx).  
Code definition to be parsed: Antenna-Radome

2) ngs14.atx  
Source link:  [https://www.ngs.noaa.gov/ANTCAL/LoadFile?file=ngs14.atx](https://www.ngs.noaa.gov/ANTCAL/LoadFile?file=ngs14.atx).  
Code definition to be parsed: Antenna-Radome

3) PCV_COD.I14  
Source link:  [http://ftp.aiub.unibe.ch/BSWUSER52/GEN/PCV_COD.I14](http://ftp.aiub.unibe.ch/BSWUSER52/GEN/PCV_COD.I14).  
Code definition to be parsed: Antenna-Radome

4) RECEIVER.  
Source link:  [http://ftp.aiub.unibe.ch/BSWUSER52/GEN/RECEIVER.](http://ftp.aiub.unibe.ch/BSWUSER52/GEN/RECEIVER.).  
Code definition to be parsed: Receiver

5) rcvr_ant.tab  
Source link:  [https://files.igs.org/pub/station/general/rcvr_ant.tab](https://files.igs.org/pub/station/general/rcvr_ant.tab).  
Code definition to be parsed: Receiver
